class CreateRoPtags < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_ptags do |t|
      t.string :package_tag_name
      t.string :description
    end
  end
end
