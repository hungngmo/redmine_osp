class CreateRoTestcases < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_testcases do |t|
      t.string :testcase_name
      t.text :precondition
      t.text :postaction
      t.integer :status, limit: 2, default: 1 # 1: init, 2: public, 0: delete
      t.integer :creator_id
      t.integer :reviewer_id
      t.datetime :review_date
      t.integer :package_id
    end
  end
end
