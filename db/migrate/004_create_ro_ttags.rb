class CreateRoTtags < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_ttags do |t|
      t.string :testcase_tag_name
      t.string :description
    end
  end
end
