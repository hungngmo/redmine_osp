class CreateRoPackageTags < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_package_tags do |t|
      t.integer :package_id
      t.integer :ptag_id
    end
  end
end
