class CreateRoPackages < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_packages do |t|
      t.string :package_name
      t.string :description
      t.integer :status, limit: 2, default: 1 # 1: active, 2: closed
    end
  end
end
