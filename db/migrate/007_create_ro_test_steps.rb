class CreateRoTestSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_test_steps do |t|
      t.string :command
      t.boolean :exactly, default: :true
      t.string :expected_output
      t.integer :order
      t.integer :testcase_id
    end
  end
end
