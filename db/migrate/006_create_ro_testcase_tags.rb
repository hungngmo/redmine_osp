class CreateRoTestcaseTags < ActiveRecord::Migration[5.2]
  def change
    create_table :ro_testcase_tags do |t|
      t.integer :testcase_id
      t.integer :ttag_id
    end
  end
end
