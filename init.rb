Redmine::Plugin.register :redmine_osp do
  name 'Redmine Osp plugin'
  author 'Toshiba'
  description 'This is a plugin for Redmine'
  version '0.0.1'

  menu :top_menu , :ro_packages, { :controller => 'ro_packages', :action => 'index' }, :caption => 'Packages'
end
