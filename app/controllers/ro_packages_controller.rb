class RoPackagesController < ApplicationController
  layout "osp"
  before_action :set_package, only: [:edit, :update, :show, :destroy]

  def index
    @packages = RoPackage.where(status: :active).order(:package_name)
  end

  def new
    @package = RoPackage.new
  end

  def create
    RoPackage.create package_params

    redirect_to ro_packages_path
  end

  def edit
    #
  end

  def update
    @package.update(package_params)

    redirect_to ro_packages_path
  end

  def show
    #
  end

  private

  def package_params
    params.require(:ro_package).permit(:package_name, :description)
  end

  def set_package
    @package = RoPackage.find(params[:id])
  end
end
