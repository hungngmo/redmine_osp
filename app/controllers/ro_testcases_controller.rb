class RoTestcasesController < ApplicationController
  layout "osp"
  before_action :set_testcase, only: [:edit, :update, :show, :destroy]

  def index
    @testcases = RoTestcase.all
  end

  def new
    @testcase = RoTestcase.new
  end

  def create
    RoTestcase.create(testcase_params)
  end

  def edit
    #
  end

  def update
    #
  end

  def show
    #
  end

  private

  def testcase_params
    params.require(:ro_testcase).permit(:testcase_name, :precondition, :postaction, :status, :package_id)
  end

  def set_testcase
    @testcase = RoTestcase.find(params[:id])
  end
end
