class RoPackage < ActiveRecord::Base
  # 1: active, 2: closed
  enum status: { active: 1, closed: 2 }
end
